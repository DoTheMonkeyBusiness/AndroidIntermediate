package com.nasalevich.androidintermidiate.mainPage.data.entity

data class CatEntity(
    val id: Int,
    val name: String,
    val description: String
)
