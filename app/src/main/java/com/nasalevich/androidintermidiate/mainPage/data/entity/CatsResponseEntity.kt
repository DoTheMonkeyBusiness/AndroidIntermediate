package com.nasalevich.androidintermidiate.mainPage.data.entity

data class CatsResponseEntity(val cats: List<CatEntity>)
