package com.nasalevich.androidintermidiate.mainPage.domain.model

data class CatModel(
    val id: Int,
    val name: String,
    val description: String
)
